
import asyncio
import discord
import io
import os

from gettoken import GetToken
from logger import GetLogger
from tinytag import TinyTag

log = GetLogger()

CHANNEL_ID = '414755754604757003'
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
FILE_PATH = os.path.join(BASE_PATH, 'gtts.mp3')

if not os.path.exists(FILE_PATH):
    open(FILE_PATH, 'a').close()


if not discord.opus.is_loaded():
    # the 'opus' library here is opus.dll on windows
    # or libopus.so on linux in the current directory
    # you should replace this with the location the
    # opus library is located in and with the proper filename.
    # note that on windows this DLL is automatically provided for you
    discord.opus.load_opus('opus')


async def on_ready():
    await client.wait_until_ready()

    channel = discord.Object(id=CHANNEL_ID)
    voice = await client.join_voice_channel(channel)

    start_time = os.path.getmtime(FILE_PATH)
    log.info('File Start Time: {}'.format(start_time))

    while True:
        updated_time = os.path.getmtime(FILE_PATH)
        log.debug('Checking file. Current append time: {}'.format(updated_time))
        if updated_time > start_time:
            log.info('TTS File updated. Time: {}'.format(updated_time))
            start_time = updated_time
            player = voice.create_ffmpeg_player(FILE_PATH)
            tag = TinyTag.get(FILE_PATH)
            player.start()
            while not player.is_done():
                await asyncio.sleep(tag.duration + 1)
        await asyncio.sleep(1)


if __name__ == '__main__':
    client = discord.Client()
    client.loop.create_task(on_ready())
    client.run(GetToken('DISCORD_VOICE_TOKEN'))
