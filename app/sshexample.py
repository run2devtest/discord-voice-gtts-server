import os
from time import sleep
from speech_client import send_voice_msg

from sshtunnel import SSHTunnelForwarder, create_logger
from logger import GetLogger

log = GetLogger()

import socket

HOST_ADDRESS = ''
USERNAME = ''
PORT = 6667


def send_message(address):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(address)
    sleep(1)

    message = 'testing'
    client.send(message.encode())
    # receive the response data (4096 is recommended buffer size)
    response = client.recv(4096)

    if response.decode() == 'ACK!':
        print('message sent')
    else:
        print('message not sent', response.decode())


while True:
    with SSHTunnelForwarder(
            HOST_ADDRESS,
            ssh_username=USERNAME,
            remote_bind_address=('0.0.0.0', PORT),
            logger=create_logger(loglevel=1)) as tunnel:

        send_message(tunnel.local_bind_address)
    sleep(1)


# work with `SECRET SERVICE` through `server.local_bind_port`.
