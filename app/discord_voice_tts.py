import socket
import os
import random

from gtts import gTTS
from logger import GetLogger
from time import sleep
from tinytag import TinyTag


log = GetLogger()

bind_ip = '0.0.0.0'
bind_port = 6666

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5)  # max backlog of connections

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
FILE_PATH = os.path.join(BASE_PATH, 'gtts.mp3')

log.info('Listening on {}:{}'.format(bind_ip, bind_port))


def create_tts_file(message):
    try:
        text = message
        log.info('Received {}'.format(text))

        language = random.choice(['en-uk', 'en-us', 'en-au'])

        tts = gTTS(text=text, lang=language, slow=False)
        tts.save(FILE_PATH)
    except Error as e:
        log.info('error {}'.format(e))


def handle_client_connection(client_socket):
    request = client_socket.recv(1024)
    client_socket.send(b'ACK!')
    client_socket.close()
    create_tts_file('{}'.format(request.decode()))
    tag = TinyTag.get(FILE_PATH)
    log.info('waiting {} seconds.'.format(tag.duration))
    sleep(tag.duration + 1)

if __name__ == '__main__':
    while True:
        client_sock, address = server.accept()
        print('Accepted connection from {}:{}'.format(address[0], address[1]))
        try:
            handle_client_connection(client_sock)
        except UnicodeDecodeError:
            print("UnicodeDecodeError: 'utf-8' codec can't decode byte 0xe0 in position 5: invalid continuation byte")
        sleep(2.5)
